import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class ReqresInTests {
    @Test
    public void test() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("https://reqres.in/api/users/1"))
                .build();
        try {
            String json = client.send(request, HttpResponse.BodyHandlers.ofString()).body();
            String expectedOutput = "{\"data\":{\"id\":1,\"email\":\"george.bluth@reqres.in\",\"first_name\":\"George\"," +
                    "\"last_name\":\"Bluth\",\"avatar\":\"https://reqres.in/img/faces/1-image.jpg\"},\"support\":{" +
                    "\"url\":\"https://reqres.in/#support-heading\",\"text\":\"To keep ReqRes free, contributions towards server costs are appreciated!\"" +
                    "}}";
            Assertions.assertEquals(expectedOutput, json, "Check /user/{id} endpoint with id = 1");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
